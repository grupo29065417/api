module.exports = class usuarioController {


    static async postUser (req, res){
        const { usuario, senha, cpf, email } = req.body

      

        const letraMaiuscula = /[A-Z]/;  

        if ( usuario === "" || cpf === "" || email === "" || senha === "")
        {
            res.status(200).json({ message: 'Campos inválidos. Por favor, digite novamente'})
            return;
        }

        if ( usuario.length < 5 )
        {
            res.status(200).json({ message: 'Digite um usuário com no mínimo 5 caracteres.'})
            return;
        }

        if ( !letraMaiuscula.test(usuario) )
        {
            res.status(200).json({ message: 'O usuário precisa ter o início da letra maiúscula.'})
            return;
        }

        if ( senha.length < 6 )
        {
            res.status(200).json({ message: 'Digite uma senha com no mínimo 6 caracteres.'})
            return;
        }

        const simboloEspecial = /[!@#$%^&*]/;

        if ( !simboloEspecial.test(senha) )
        {
            res.status(200).json({ message: 'Sua senha precisa ter no mínimo um símbolo especial.'})
            return;
        }
        

        if ( !letraMaiuscula.test(senha) ) 
        {
            res.status(200).json({ message: 'Sua senha precisa ter no mínimo uma letra maiúscula.'})
            return;
        }

        if (cpf.length !== 11)
         {
            res.status(200).json({ message: 'Seu CPF está inválido.'})
            return;
         }

         const simbolosEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        if (!simbolosEmail.test(email))
        {

            res.status(200).json({ message: "Por favor, insira um endereço de email válido." });
            return;

        }
        else {
            res.status(200).json({ message: 'Usuário cadastrado com sucesso.'})
            console.log('Usuário cadastrado com os seguintes dados:', req.body);
        }


}

   /* static async getUsers(req, res) {

        const usuarios = await Usuario.findAll(); // Importaríamos o modelo Usuario e procuraríamos ele no banco de dados
        
        if (usuarios.length === 0) {

          return res.status(200).json({ message: "Nenhum usuário cadastrado." });
           
        }

        res.status(200).json({ usuarios }); 
        
    }  */



static async putUser (req, res){
    const { usuario, senha, cpf, email } = req.body

    // Validações dos campos de usuário, cpf, email e senha logo abaixo 

    const letraMaiuscula = /[A-Z]/;  

    if ( usuario === "" || cpf === "" || email === "" || senha === "")
    {
        res.status(200).json({ message: 'Campos inválidos. Por favor, digite novamente'})
        return;
    }

    if ( usuario.length < 5 )
    {
        res.status(200).json({ message: 'Reescreva seu usuário com no mínimo 5 caracteres.'})
        return;
    }

    if ( !letraMaiuscula.test(usuario) )
    {
        res.status(200).json({ message: 'Você precisa ter o início do seu usuário com letra maiúscula.'})
        return;
    }

    if ( senha.length < 6 )
    {
        res.status(200).json({ message: 'Reescreva uma senha com no mínimo 6 caracteres.'})
        return;
    }

    const simboloEspecial = /[!@#$%^&*]/;

    if ( !simboloEspecial.test(senha) )
    {
        res.status(200).json({ message: 'Sua senha precisa ter no mínimo um símbolo especial.'})
        return;
    }
    

    if ( !letraMaiuscula.test(senha) ) 
    {
        res.status(200).json({ message: 'Sua senha precisa ter no mínimo uma letra maiúscula.'})
        return;
    }

    if (cpf.length !== 11)
     {
        res.status(200).json({ message: 'Seu CPF está inválido.'})
        return;
     }

     const simbolosEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!simbolosEmail.test(email))
    {

        res.status(200).json({ message: "Por favor, atualize seu usuário com um endereço de email válido." });
        return;

    }
    else {
        res.status(200).json({ message: 'Usuário atualizado com sucesso.'})
        console.log('Usuário foi atualizado com os seguintes dados:', req.body);
    }


}

static async deleteUser(req, res){
    console.log(req.params.id)
    if( req.params.id !== "")
    {
      res.status(200).json({ message: " O usuário foi deletado. " });
    }
    else{
      res.status(200).json({ message: " Usuário não encontrado. " });
    }

  }


  static async logUser(req, res) {
    const { usuario, senha } = req.body;

 
    if (usuario === 'adm' && senha === '1234567') {
        res.status(200).json({ message: "Usuário logado com sucesso." });
    }
     else {
        res.status(401).json({ message: "Usuário ou senha incorretos." });
    }
}

  static async signInUser(req, res) {
    const { usuario, senha, confirmSenha, email } = req.body

    if(usuario === 'adm' && senha === '1234567' && confirmSenha === '1234567' && email === 'vito@gmail.com' )
    {
        res.status(200).json({ message: "Usuário cadastrado com sucesso." });
    }
    else {
        res.status(401).json({ message: "Usuário ou senha incorretos." });
    }

}



}

